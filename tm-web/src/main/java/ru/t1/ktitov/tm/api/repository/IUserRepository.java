package ru.t1.ktitov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.ktitov.tm.model.User;

@Repository
public interface IUserRepository extends JpaRepository<User, String> {

    User findByLogin(@NotNull final String login);

    User findFirstById(@NotNull final String id);

}
