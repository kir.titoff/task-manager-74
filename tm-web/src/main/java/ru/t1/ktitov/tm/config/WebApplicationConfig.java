package ru.t1.ktitov.tm.config;

import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.t1.ktitov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktitov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktitov.tm.api.endpoint.ITaskEndpoint;

import javax.xml.ws.Endpoint;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebApplicationConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private Bus bus;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/services/*").permitAll()

                .and()
                .authorizeRequests()

                .anyRequest().authenticated()
                .and()
                .formLogin()

                .and()
                .logout().permitAll()
                .logoutSuccessUrl("/login")

                .and()
                .csrf().disable();
    }

    @Bean
    public Endpoint authEndpointRegistry(final IAuthEndpoint authEndpoint, Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, authEndpoint);
        endpoint.publish("/AuthEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint projectEndpointRegistry(final IProjectEndpoint projectEndpoint, Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, projectEndpoint);
        endpoint.publish("/ProjectEndpoint");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpointRegistry(final ITaskEndpoint taskEndpoint, Bus bus) {
        final EndpointImpl endpoint = new EndpointImpl(bus, taskEndpoint);
        endpoint.publish("/TaskEndpoint");
        return endpoint;
    }

}
