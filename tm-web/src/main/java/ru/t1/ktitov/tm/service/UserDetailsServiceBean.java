package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.IUserRepository;
import ru.t1.ktitov.tm.dto.CustomUser;
import ru.t1.ktitov.tm.enumerated.RoleType;
import ru.t1.ktitov.tm.model.Role;
import ru.t1.ktitov.tm.model.User;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @Autowired(required = false)
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {
        final User user = findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);

        final List<Role> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        for (final Role role : userRoles) roles.add(role.toString());

        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMIN);
        initUser("test", "test", RoleType.USER);
    }

    public User findByLogin(@NotNull final String login) {
        return userRepository.findByLogin(login);
    }

    private void initUser(
            @NotNull final String login,
            @NotNull final String password,
            @NotNull final RoleType roleType
    ) {
        final User user = findByLogin(login);
        if (user != null) return;
        createUser(login, password, roleType);
    }

    @Transactional
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (roleType == null) return;
        final String passwordHash = passwordEncoder.encode(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);
        user.setRoles(Collections.singletonList(role));
        userRepository.save(user);
    }

}
