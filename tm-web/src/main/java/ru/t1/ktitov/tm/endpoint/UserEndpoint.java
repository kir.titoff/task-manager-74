package ru.t1.ktitov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktitov.tm.api.endpoint.IUserEndpoint;
import ru.t1.ktitov.tm.model.User;
import ru.t1.ktitov.tm.service.UserService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/users")
@WebService(endpointInterface = "ru.t1.ktitov.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    @Autowired
    private UserService userService;

    @Override
    @WebMethod
    @PutMapping("/save")
    public void save(
            @WebParam(name = "user", partName = "user")
            @RequestBody final User user
    ) {
        userService.update(user);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @WebParam(name = "user", partName = "user")
            @RequestBody final User user
    ) {
        userService.remove(user);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        userService.removeById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<User> findAll() {
        return userService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public User findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return userService.findOneById(id);
    }

}
