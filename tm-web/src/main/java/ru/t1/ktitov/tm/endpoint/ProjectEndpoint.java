package ru.t1.ktitov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ktitov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.service.ProjectService;
import ru.t1.ktitov.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.ktitov.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.update(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.remove(UserUtil.getUserId(), project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        projectService.removeById(UserUtil.getUserId(), id);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        projectService.removeAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAll(UserUtil.getUserId());
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.findOneById(UserUtil.getUserId(), id);
    }

}
