package ru.t1.ktitov.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ktitov.tm.dto.CustomUser;
import ru.t1.ktitov.tm.enumerated.Status;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.model.Task;
import ru.t1.ktitov.tm.service.ProjectService;
import ru.t1.ktitov.tm.service.TaskService;

import java.util.Collection;

@Controller
public final class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @GetMapping("/task/create")
    public String create(@AuthenticationPrincipal final CustomUser user) {
        @NotNull Task task = new Task("New Task " + System.currentTimeMillis());
        task.setUserId(user.getUserId());
        taskService.update(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.removeById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") Task task,
            BindingResult result
    ) {
        if (task.getProjectId() == null || task.getProjectId().isEmpty()) task.setProjectId(null);
        task.setUserId(user.getUserId());
        taskService.update(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        final Task task = taskService.findOneById(user.getUserId(), id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", getProjects(user.getUserId()));
        modelAndView.addObject("statuses", getStatuses());
        return modelAndView;
    }

    private Collection<Project> getProjects(@NotNull final String id) {
        return projectService.findAll(id);
    }

    private Status[] getStatuses() {
        return Status.values();
    }

}
