package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.api.repository.IProjectRepository;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Transactional
    public Project add(@Nullable final String userId, @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        repository.save(model);
        return model;
    }

    @NotNull
    @Transactional
    public Collection<Project> add(@Nullable final String userId, @Nullable final Collection<Project> models) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (models == null) throw new EntityNotFoundException();
        models.forEach(m -> m.setUserId(userId));
        repository.saveAll(models);
        return models;
    }

    @NotNull
    @Transactional
    public Project update(@Nullable final String userId, @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        model.setUpdated(new Date());
        repository.saveAndFlush(model);
        return model;
    }

    @Nullable
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        return repository.findByUserId(userId);
    }

    @Nullable
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        return repository.findFirstByUserIdAndId(userId, id);
    }

    @NotNull
    @Transactional
    public Project remove(@Nullable final String userId, @Nullable final Project model) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (model == null) throw new EntityNotFoundException();
        repository.delete(model);
        return model;
    }

    @NotNull
    @Transactional
    public Project removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (id == null || id.isEmpty()) throw new EntityNotFoundException();
        Optional<Project> model = repository.findById(id);
        if (!userId.equals(model.get().getUserId())) throw new EntityNotFoundException();
        repository.deleteById(id);
        return model.get();
    }

    @NotNull
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        repository.deleteByUserId(userId);
    }

    @NotNull
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new EntityNotFoundException();
        if (name == null || name.isEmpty()) throw new EntityNotFoundException();
        @NotNull final Project project = new Project();
        project.setName(name);
        add(userId, project);
        return project;
    }

}
