package ru.t1.ktitov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.ktitov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
@RequestMapping("/api/projects")
public interface IProjectEndpoint {

    @WebMethod
    @PostMapping("/save")
    void save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    );

    @WebMethod
    @DeleteMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    );

    @WebMethod
    @DeleteMapping("/delete/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    );

    @WebMethod
    @DeleteMapping("/deleteAll")
    void deleteAll();

    @WebMethod
    @GetMapping("/findAll")
    Collection<Project> findAll();

    @WebMethod
    @GetMapping("/find/{id}")
    Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    );

}
