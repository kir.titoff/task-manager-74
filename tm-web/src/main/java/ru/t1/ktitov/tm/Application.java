package ru.t1.ktitov.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * http://localhost:8080/swagger-ui.html#
 *
 * http://localhost:8080/services
 * http://localhost:8080/services/ProjectEndpoint?wsdl
 * http://localhost:8080/services/TaskEndpoint?wsdl
 */

@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
