package ru.t1.ktitov.tm.dto.response.system;

import lombok.Getter;
import lombok.Setter;
import ru.t1.ktitov.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ServerVersionResponse extends AbstractResponse {

    private String version;

}
